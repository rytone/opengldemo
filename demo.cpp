#include <stdio.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>

#include "stb_vorbis.h"

short* audio_pos;
int32_t audio_len;

void audioCallback(void* userdata, Uint8* stream, int len) {
	// convert stream to 16 bits (16 bit audio)
	short* stream16 = (short*)stream;
	// if there is no time remaining, return
	if (audio_len == 0) return;
	// if sdl wants more bytes than we have, then just set len to our remaining bytes
	len = (len > audio_len ? audio_len : len);

	// copy a (frame, sample?) to the audio buffer
	SDL_memcpy(stream16, audio_pos, len);
	// increment the pointer location by the amount we copied
	audio_pos += len / sizeof(short);
	// decrease the remaining count of bytes
	audio_len -= len;
}

int main(int argc, char *argv[]) {
	SDL_Window* window;
	SDL_GLContext context;
	short* decoded;
#pragma omp parallel
	{
#pragma omp sections
		{
#pragma omp section
			{
				printf("loading music...\n");
				int channels, rate, len;
				len = stb_vorbis_decode_filename("FRUHLING.ogg", &channels, &rate, &decoded);
				len *= 2 * channels;

				SDL_AudioSpec spec;
				spec.freq = rate;
				spec.format = AUDIO_S16;
				spec.channels = channels;
				spec.samples = 2048;
				spec.callback = audioCallback;
				spec.userdata = NULL;

				audio_pos = decoded;
				audio_len = len;

				if (SDL_OpenAudio(&spec, NULL) < 0) {
					printf("error opening audio device.\n");
					SDL_GL_DeleteContext(context);
					SDL_Quit();
					free(decoded);
				} else {
					SDL_PauseAudio(0);
					printf("playback started\n");
				}
			}
#pragma omp section
			{
				printf("creating context...\n");

				SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO);

				SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
				SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
				SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
				SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);

				window = SDL_CreateWindow("demo", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1280, 720, SDL_WINDOW_OPENGL);
				context = SDL_GL_CreateContext(window);
				printf("entering render loop\n");
				SDL_Event windowEvent;
				while (true) {
					if (SDL_PollEvent(&windowEvent)) {
						if (windowEvent.type == SDL_QUIT) break;
					}

					SDL_GL_SwapWindow(window);
				}
			}
		}
	}

	printf("shutdown...\n");
	SDL_GL_DeleteContext(context);
	SDL_Quit();
	free(decoded);

	return 0;
}
